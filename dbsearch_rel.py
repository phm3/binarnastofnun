#!/usr/bin/python
# -*- coding: utf-8 -*-
# this is a python based interface for bin.arnastofa.is

# import sql to slite3 database
#  cat SHsnid.sql | sqlite3 bin.db


import sqlite3 as lite
import sys, os, re
import pprint
from terminaltables import AsciiTable
import itertools

# colors
def prRed(prt): print("\033[91m{}\033[00m" .format(prt))
def prGreen(prt): print("\033[92m{}\033[00m" .format(prt))
def prYellow(prt): print("\033[93m{}\033[00m" .format(prt))
def prLightPurple(prt): print("\033[94m{}\033[00m" .format(prt))
def prPurple(prt): print("\033[95m{}\033[00m" .format(prt))
def prCyan(prt): print("\033[96m{}\033[00m" .format(prt))
def prLightGray(prt): print("\033[97m{}\033[00m" .format(prt))
def prBlack(prt): print("\033[98m{}\033[00m" .format(prt))


if len(sys.argv) < 2:

    print "No search word!"
    sys.exit()
else:
    word = sys.argv[1].decode("utf-8")

    con = lite.connect('raw/bin.db')

# connect to the database

with con:

    cur = con.cursor()

    # can add beygingarmynd to specify word TODO
    cur.execute("SELECT * FROM bin WHERE uppflettiord =:word", {"word": word})

    rows = cur.fetchall()

    cur.execute("SELECT * FROM bin WHERE uppflettiord = :word limit 1",
                {"word": word})

    single = cur.fetchone()
    os.system('clear')

    # determine if there is anything in the db for searched word
    
    if single == None:
        sys.exit("Nothing found!")

    

    for row in single:
        if single[2] == "so":
            
            
            array_present = []
            array_past = []
            array_sub_present = []
            array_sub_past = []
            past_present_t_table_data = []

            

            sagnb_midmynd = ""

            
            for row in rows:
                

                if "GM-NH" in row[5]:
                    verb = row[4]

                # get the values
                if "GM-FH-NT" in row[5] and "OP-GM-FH-NT" not in row[5] and "OP-GM-VH-NT" not in row[5] and "OP-GM-VH-ÞT".decode("utf-8") not in row[5]:
                    
                    array_present.append([row[5].replace("GM-FH-NT-".decode("utf-8"), ""),row[4]])
                    
                  

                if "GM-FH-ÞT".decode("utf-8") in row[5] and "OP-GM-FH-NT" not in row[5] and "OP-GM-VH-NT" not in row[5] and "OP-GM-VH-ÞT".decode("utf-8") not in row[5]:
                    
                    array_past.append([row[5].replace("GM-FH-ÞT-".decode("utf-8"), ""),row[4]])


                if "GM-VH-NT".decode("utf-8") in row[5]:
                    
                    array_sub_present.append([row[5].replace("GM-VH-NT-".decode("utf-8"),""), row[4]])


                if "GM-VH-ÞT".decode("utf-8") in row[5]:
                    array_sub_past.append([row[5].replace("GM-VH-ÞT-".decode("utf-8"),""), row[4]])

                if "GM-SAGNB".decode("utf-8") in row[5]:
                    sagnb_germynd = row[4]


                if "MM-SAGNB".decode("utf-8") in row[5]:
                   
                    sagnb_midmynd = row[4]
                    
                
                
                
           
            prCyan(verb)
            
            

            
            past_present_t_table_data += [['','PRESENT','PAST']]
            for k,v in zip(array_past, array_present):
                
                 past_present_t_table_data += [[k[0],v[1],k[1]]]
                
            

            

            sagnbot_t_table_data = [["Germynd".decode("utf-8"), "Miðmynd".decode("utf-8")],[sagnb_germynd, sagnb_midmynd]]

           
            
            
            present_past_t_table = AsciiTable(past_present_t_table_data)
            sagnbot_t_table = AsciiTable(sagnbot_t_table_data)

            print""
            prYellow("Nútíð / Þátíð")
            print present_past_t_table.table
            
            # sagnbot
            prYellow("Sagnbót")
            print sagnbot_t_table.table
            sys.exit("detected verb")
        
    if single[2] == "kk" or single[2] == "kvk" or single[2] == "hk":
        print "Gender:", single[2]
# get  EINTALA

print
print "Eintala"
print

words = []

for row in rows:
    if "ET" in row[5] and "ETgr" not in row[5]:
       
        case = row[5].replace("ET", "")
        word = row[4]
    if "ETgr" in row[5]:
        
        alt_case = row[5]
        alt_word = row[4]
        #
        print case.ljust(3), "|", word.rjust(8), "|", alt_word

# get FLEIRTALA
print
print "Fleirtala"
print

for row in rows:
    if "FT" in row[5] and "FTgr" not in row[5]:
        case = row[5].replace("FT", "")
        word = row[4]
    if "FTgr" in row[5]:
        alt_case = row[5]
        alt_word = row[4]

        print case.ljust(3), "|", word.rjust(8), "|", alt_word
