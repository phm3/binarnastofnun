Python wrapper for BIN
====

Basic python search script for words in: http://bin.arnastofnun.is

#### What is required?

* You need to obtain the flat files with the word data
* Convert it to the sql flat file: `cat SHsnid.sql | sqlite3 bin.db`
* Install python modules: `terminaltables` 

Important
====

What you see is what you get... `góða skemmtun`
